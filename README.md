# GraphQL Address

This module adds GraphQL support to the address module.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/graphql_address).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/graphql_address).


## Requirements

This module requires the following modules:

- [GraphQL](https://www.drupal.org/project/graphql)
- [Address](https://www.drupal.org/project/address)
- [Typed Data API enhancements](https://www.drupal.org/project/typed_data)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the "GraphQL Address" SchemaExtension on your graphQL server edit
   page.
2. Configure any Address field on an entity to be resolved by the provided
   'graphql_address_field_values' DataProducer.
3. Include the field on the relevant Type to resolve as `AddressAddress`

Or if you prefer, you can replace or extend the included SchemaExtension with
your own code, **the DataProducers should still be useful to you**.
