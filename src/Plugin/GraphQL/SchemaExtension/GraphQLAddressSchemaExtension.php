<?php

namespace Drupal\graphql_address\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
 * Schema Extension for Australian Business taxonomy term type.
 *
 * @SchemaExtension(
 *   id = "graphql_address_extension",
 *   name = "GraphQL Address",
 *   description = "Provides an Address type compatible with fields created
 *     using the Address module.",
 *   schema = "graphql_address",
 * )
 */
class GraphQLAddressSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * All of the components that can be extracted from an Address field.
   *
   * @var string[]
   */
  protected $fieldComponents = [
    'country_code',
    'administrative_area',
    'locality',
    'dependent_locality',
    'postal_code',
    'sorting_code',
    'address_line1',
    'address_line2',
    'organization',
    'given_name',
    'additional_name',
    'family_name',
  ];

  /**
   * Some useful methods from the country repository.
   *
   * @var string[]
   */
  protected $countryFieldMethods = [
    'country_code' => 'getCountryCode',
    'name' => 'getName',
    'currency' => 'getCurrencyCode',
  ];

  protected $formatRepositoryMethods = [
    'locale' => 'getLocale',
    'format' => 'getFormat',
    'local_format' => 'getLocalFormat',
    'used_fields' => 'getUsedFields',
    'required_fields' => 'getRequiredFields',
    'uppercase_fields' => 'getUppercaseFields',
    'administrative_area_type' => 'getAdministrativeAreaType',
    'locality_type' => 'getLocalityType',
    'dependent_locality_type' => 'getDependentLocalityType',
    'postal_code_type' => 'getPostalCodeType',
    'postal_code_pattern' => 'getPostalCodePattern',
    'postal_code_prefix' => 'getPostalCodePrefix',
    'subdivision_depth' => 'getSubdivisionDepth',
  ];

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver('Query', 'address_country',
      $builder->compose(
        $builder->context('graphql_address_langcode', $builder->fromArgument('langcode')),
        $builder->callback(function ($parent, $args) {
          return $args['country_code'];
        })
      )
    );

    $registry->addFieldResolver('Query', 'address_countries',
      $builder->compose(
        $builder->context('graphql_address_langcode', $builder->fromArgument('langcode')),
        $builder->produce('graphql_address_country_repository')
      )
    );

    $registry->addFieldResolver('AddressAddress', 'country',
      $builder->produce('graphql_address_field_component')
        ->map('address', $builder->fromParent())
        ->map('component_name', $builder->fromValue('country_code'))
    );

    foreach ($this->fieldComponents as $component) {
      $registry->addFieldResolver('AddressAddress', $component,
        $builder->produce('graphql_address_field_component')
          ->map('address', $builder->fromParent())
          ->map('component_name', $builder->fromValue($component))
      );
    }

    foreach ($this->countryFieldMethods as $field => $method) {
      $registry->addFieldResolver('AddressCountry', $field,
        $builder->produce('graphql_address_country_fields')
          ->map('country_code', $builder->fromParent())
          ->map('method', $builder->fromValue($method))
          ->map('langcode', $builder->fromContext('graphql_address_langcode'))
      );
    }

    foreach ($this->formatRepositoryMethods as $field => $method) {
      $registry->addFieldResolver('AddressCountry', $field,
        $builder->produce('graphql_address_format_repository')
          ->map('country_code', $builder->fromParent())
          ->map('method', $builder->fromValue($method))
          ->map('langcode', $builder->fromContext('graphql_address_langcode'))
      );
    }

    $registry->addFieldResolver('AddressCountry', 'subdivisions',
      $builder->produce('graphql_address_subdivisions')
        ->map('country_code', $builder->fromParent())
        ->map('langcode', $builder->fromContext('graphql_address_langcode'))
    );

  }

}
