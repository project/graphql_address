<?php

namespace Drupal\graphql_address\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "graphql_address_field_values",
 *   name = @Translation("Address field values"),
 *   description = @Translation("Returns the array of address field values for a cardinality=1 field, or an array of arrays for any other cardinality."),
 *   produces = @ContextDefinition("array",
 *     label = @Translation("Address field values")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity")
 *     ),
 *     "field_name" = @ContextDefinition("string",
 *       label = @Translation("Field name")
 *     ),
 *   }
 * )
 *
 * This is just a generic producer, it could easily be replaced by a
 * standardised entity field resolver if one were made.
 */
class AddressFieldValues extends DataProducerPluginBase {

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array|null
   */
  public function resolve(EntityInterface $entity, string $field_name): ?array {
    $field = $entity->get($field_name);
    $cardinality = $field->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();
    if ($field->access('view')) {
      $values = $field->getValue();
      if (!empty($values)) {
        if ($cardinality === 1) {
          return $values[0];
        }
        return $values;
      }
    }
    return NULL;
  }

}
