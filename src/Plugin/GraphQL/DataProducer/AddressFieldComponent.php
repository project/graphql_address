<?php

namespace Drupal\graphql_address\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "graphql_address_field_component",
 *   name = @Translation("Address field component"),
 *   description = @Translation("Resolve the component within an Address field."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Component value")
 *   ),
 *   consumes = {
 *     "address" = @ContextDefinition("any",
 *       label = @Translation("The address field.")
 *     ),
 *     "component_name" = @ContextDefinition("string",
 *       label = @Translation("Component name")
 *     ),
 *   }
 * )
 */
class AddressFieldComponent extends DataProducerPluginBase {

  /**
   * @param array $address
   * @param string $component_name
   *
   * @return string|null
   */
  public function resolve(array $address, string $component_name): ?string {
    return $address[$component_name] ?? NULL;
  }

}
