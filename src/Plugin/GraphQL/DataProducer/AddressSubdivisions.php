<?php

namespace Drupal\graphql_address\Plugin\GraphQL\DataProducer;

use CommerceGuys\Addressing\Subdivision\SubdivisionRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "graphql_address_subdivisions",
 *   name = @Translation("Subdivisions"),
 *   description = @Translation("Resolve the country_code into subdivisions."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Subdivisions")
 *   ),
 *   consumes = {
 *     "country_code" = @ContextDefinition("string",
 *       label = @Translation("The country code.")
 *     ),
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("An ISO 639-1 language code."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddressSubdivisions extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \CommerceGuys\Addressing\Subdivision\SubdivisionRepositoryInterface
   */
  protected $subdivisionRepository;

  /**
   * AddressSubdivisionName constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \CommerceGuys\Addressing\Subdivision\SubdivisionRepositoryInterface $subdivisionRepository
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SubdivisionRepositoryInterface $subdivisionRepository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->subdivisionRepository = $subdivisionRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('address.subdivision_repository')
    );
  }

  /**
   * @param string $country_code
   * @param string|null $langcode
   *
   * @return array
   */
  public function resolve(string $country_code, ?string $langcode): array {
    $subdivisions = $this->subdivisionRepository->getList([$country_code], $langcode);
    $output = [];
    foreach ($subdivisions as $code => $name) {
      $output[] = [
        'code' => $code,
        'name' => $name,
      ];
    }
    return $output;
  }

}
