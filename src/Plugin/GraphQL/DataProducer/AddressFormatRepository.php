<?php

namespace Drupal\graphql_address\Plugin\GraphQL\DataProducer;

use CommerceGuys\Addressing\AddressFormat\AddressFormatRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "graphql_address_format_repository",
 *   name = @Translation("Format Repository"),
 *   description = @Translation("Resolve the country_code into repository values."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Format repository value")
 *   ),
 *   consumes = {
 *     "country_code" = @ContextDefinition("string",
 *       label = @Translation("The format code.")
 *     ),
 *     "method" = @ContextDefinition("string",
 *       label = @Translation("The method to fetch the repository value.")
 *     ),
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("An ISO 639-1 language code."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddressFormatRepository extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \CommerceGuys\Addressing\AddressFormat\AddressFormatRepositoryInterface
   */
  protected $formatRepository;

  /**
   * AddressFormatName constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \CommerceGuys\Addressing\AddressFormat\AddressFormatRepositoryInterface
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AddressFormatRepositoryInterface $formatRepository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formatRepository = $formatRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('address.address_format_repository')
    );
  }

  /**
   * @param string $country_code
   * @param string $method
   * @param string|null $langcode
   *
   * @return mixed
   */
  public function resolve(string $country_code, string $method, ?string $langcode) {
    $format = $this->formatRepository->get($country_code, $langcode);
    return $format->$method();
  }

}
