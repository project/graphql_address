<?php

namespace Drupal\graphql_address\Plugin\GraphQL\DataProducer;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "graphql_address_country_fields",
 *   name = @Translation("Country Fields"),
 *   description = @Translation("Resolve the country_code into fields values."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Country fields value")
 *   ),
 *   consumes = {
 *     "country_code" = @ContextDefinition("string",
 *       label = @Translation("The country code.")
 *     ),
 *     "method" = @ContextDefinition("string",
 *       label = @Translation("The method to fetch the field value.")
 *     ),
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("An ISO 639-1 language code."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddressCountryFields extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected $countryRepository;

  /**
   * AddressCountryName constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CountryRepositoryInterface $countryRepository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->countryRepository = $countryRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('address.country_repository')
    );
  }

  /**
   * @param string $country_code
   * @param string $method
   * @param string|null $langcode
   *
   * @return string|null
   */
  public function resolve(string $country_code, string $method, ?string $langcode): ?string {
    $country = $this->countryRepository->get($country_code, $langcode);
    return $country->$method();
  }

}
