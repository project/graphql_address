<?php

namespace Drupal\graphql_address\Plugin\GraphQL\DataProducer;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "graphql_address_country_repository",
 *   name = @Translation("Country Repository"),
 *   description = @Translation("Resolve the country_code into repository values."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Country repository value")
 *   ),
 *   consumes = {
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("An ISO 639-1 language code."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddressCountryRepository extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected $countryRepository;

  /**
   * AddressCountryName constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CountryRepositoryInterface $countryRepository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->countryRepository = $countryRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('address.country_repository')
    );
  }

  /**
   * @param string|null $langcode
   *
   * @return array
   */
  public function resolve(?string $langcode): array {
    return array_keys($this->countryRepository->getall($langcode));
  }

}
